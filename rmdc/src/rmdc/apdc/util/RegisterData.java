package rmdc.apdc.util;

public class RegisterData {
	
	public String email;
	public String phone;
	public String street;
	public String complement;
	public String city;
	public String zipcode;
	public String nif;
	public String cc;	
	public String password;
	
	public RegisterData() {
		
	}
	
	public RegisterData(String email, String phone, String street, String complement, String city, String zipcode, String nif, String cc, String password) {
			this.email = email;
			this.phone = phone;
			this.street = street;
			this.complement = complement;
			this.city = city;
			this.zipcode = zipcode;
			this.nif = nif;
			this.cc = cc;	
			this.password = password;
	}	
}

