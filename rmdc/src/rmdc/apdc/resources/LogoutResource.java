package rmdc.apdc.resources;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

import javax.ws.rs.DELETE;

@Path("/logout")

public class LogoutResource {
	
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public LogoutResource(){	
	}
	
	@DELETE
	@Path("/v1")
	@Consumes("text/plain")
	public Response doLogout(String tokenID){
		Transaction txn = datastore.beginTransaction();
		Key tokenKey = KeyFactory.createKey("AuthToken", tokenID);
		try {
			datastore.get(tokenKey);
			datastore.delete(tokenKey);
			txn.commit();
			return Response.ok("{}").build();		
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed logout attempt for token: " + tokenKey);
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
	}
}
