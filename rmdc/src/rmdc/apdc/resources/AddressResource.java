package rmdc.apdc.resources;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Entity;

@Path("/getAddress")

public class AddressResource {
	
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public AddressResource(){}
	
	@POST
	@Path("/v1")
	@Consumes("text/plain")
	public Response getAddress(String tokenID){
		Transaction txn = datastore.beginTransaction();
		Key tokenKey = KeyFactory.createKey("AuthToken", tokenID);
		try {
			Entity token = datastore.get(tokenKey);
			String tmp = (String) token.getProperty("user_login_email");
			txn.commit();
			
			Transaction txn2 = datastore.beginTransaction();
			Key userKey = KeyFactory.createKey("User", tmp);
			
			Entity user = datastore.get(userKey);
			String tmp2 = (String) user.getProperty("user_street");
			txn2.commit();
			
			return Response.ok(tmp2).build();		
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed search attempt for token: " + tokenKey);
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		} 
	}
}
