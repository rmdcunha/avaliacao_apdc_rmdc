captureDataLogin = function(event) {
    var data = $('form[name="login"]').jsonify();
    console.log(data);
    $.ajax({
        type: "POST",
        url: "http://1-dot-rmdc-162817.appspot.com/rest/login/v2",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        //dataType: "json",
        success: function(response) {
            if(response) {
                alert("Got token with id: " + response.tokenID);
                // Store token id for later use in localStorage
                localStorage.setItem('tokenID', response.tokenID);
                location.href=("http://1-dot-rmdc-162817.appspot.com/hellomaps.html");
            }
            else {
                alert("No response");
            }
        },
        error: function(response) {
            alert("Error: "+ response.status);
        },
        data: JSON.stringify(data)
    });

    event.preventDefault();
};

captureDataRegister = function(event) {
    var data = $('form[name="register"]').jsonify();
    console.log(data);
    $.ajax({
        type: "POST",
        url: "http://1-dot-rmdc-162817.appspot.com/rest/register/v3",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        //dataType: "json",
        success: function(response) {
            if(response) {
                //alert("Got token with id: " + response.tokenID);
                // Store token id for later use in localStorage
                //localStorage.setItem('tokenID', response.tokenID);
            	alert("accept");//mandar para o html login
            	location.href=("http://1-dot-rmdc-162817.appspot.com/restfrontend.html"); 
            }
            else {
                alert("No response");
            }
        },
        error: function(response) {
            alert("Error: "+ response.status);
        },
        data: JSON.stringify(data)
    });

    event.preventDefault();
};

window.onload = function() {
    var frms = $('form[name="login"]');     //var frms = document.getElementsByName("login");
    var frms2 = $('form[name="register"]');
    frms[0].onsubmit = captureDataLogin;        
    frms2[0].onsubmit = captureDataRegister;
}